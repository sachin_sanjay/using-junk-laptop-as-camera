# README #

This is how i used my junk laptop as a camera for my OSCP evaluation cause i could not afford one 

### Journey ###

I needed a camera that could help me in my oscp exam but i had already spent a lot of money, I stummbled upon a video of Linus from LTT stating something about IP camera streaming a few month ago.
I use **Arch Linux** as my main OS and had to figure out how to connect IP to /dev/video unfortunately most of the tools created are for windows so no luck there. I had to resort to google for help and an hour or so i found out that **VLC** the media player can stream input from a captured device which actually blew my mind cause the last time I used VLC was like 3 years ago since i watch most of my stuff through web browser.

NOTE: AFTER SELECTING DEVICE TO STREAM IN DESTINATION SETUP MAKE SURE TO CLICK **ADD**  AND NOT **NEXT**

![Alt text](img/vlc.png) 
---

Now comes the problem of taking a stream and mapping it to a video device.

1. First thing i did was getting into v4l2loopback so this is a module you can use to make a loopback device which can act as a video for installing in arch [just use pacman](https://www.archlinux.org/packages/community/any/v4l2loopback-dkms/) or for ubuntu based you can use apt but i read somehwhere apt repository has the outdated version and you must build from source 

1. After installing that you have to use modeprobe to start using the vl42loopback ```sudo modprobe v4l2loopback card_label="OBS Cam" exclusive_caps=1``` 

1. Now comes the mapping part which i did in 2 ways the first being ffmpeg -re -i (the http url) some settings for the stream /dev/video0 this does work when i open in VLC but for some reason chrome and cheese could not detect it 

1. So i spent a almost 3-5 hours on figuring out the issue after some digging i found the [second method](https://github.com/umlaeute/v4l2loopback/issues/183) which was using obs 

1. And after going through various website i found out [this github](https://github.com/CatxFish/obs-v4l2sink) which basically provides a plugin for obs to stream to /dev/video0

And so it finally works

### Link to go through ###

* [setting up OBS with http stream](https://www.danlewisreport.com/2017/04/windows-10-broadcast-ip-camera-live-stream-youtube/)
* [another cool setup link](https://srcco.de/posts/using-obs-studio-with-v4l2-for-google-hangouts-meet.html)
